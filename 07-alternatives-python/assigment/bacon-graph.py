# -*- coding: iso-8859-15 -*-
import csv
import pprint


movie_list=[]
actor_list= []

with open('movies-long.csv') as movie_file:
    reader = csv.reader(movie_file)
    movie_list = list(reader)
    
    for m in movie_list:
        actor_list.append(m[0])
    
movie_file.close()



def movies_with(actor):
    '''List of movies in which a given actor appears.'''
    in_these_movies = []

    for movie in movie_list:
        if movie[0] == actor:
            # rstrip is needed due to some trailing whitespace on the source
            in_these_movies.append(movie[1].rstrip())
    
    return in_these_movies



def cast(movie):
    '''List of actors for a given movie.'''
    the_cast = []
    
    for m in movie_list:

        if m[1].rstrip() == movie: # rstrip is needed due to some trailing whitespace on the source
            the_cast.append(m[0])
            
    return the_cast



def worked_with(actor):
    '''List of all work connections that an actor has.'''
    coworkers = []
    
    what_movies = movies_with(actor)

    for movie in what_movies:
            
        the_cast = cast(movie)
        
        for a in the_cast:
            if (a != actor) and (a not in coworkers):
                coworkers.append(a)

    return coworkers



def  make_graph():
    '''Gets a dictionary that represents a graph. 
    The keys of the dictionary are the actors' names, and the values are sets of work connections.
    E.g.
    ...
    'Candy, John': set(['Martin, Steve', 'McKean, Michael']),
    'Carell, Steve': set([   'Applegate, Christina',
                             'Armisen, Fred',
                             'Bacon, Kevin',
                             'Moore, Julianne',
                             'Rogen, Seth',
                             'Rudd, Paul',
                             'Stone, Emma']),
    ...
    '''
    actors_graph = {}
    
    for actor in actor_list:
        actors_graph[actor] = set(worked_with(actor))
    
    return actors_graph






######################## tests ###################

def test_actors():
   
    test_actors = ['Bacon, Kevin', 'Johansson, Scarlett', 'Basinger, Kim']
    pp = pprint.PrettyPrinter(indent=4)

    for actor in test_actors:
        print(actor + " movies: ")        
        pp.pprint(movies_with(actor))
        print('')
        print(actor + " has worked with: ")
        pp.pprint(worked_with(actor))
        print("-" * 40)
        
        
    print("=" * 60)
    

def test_cast():
    
    test_movies = ['Crazy Stupid Love','Don Jon','Boiler Room', 'Apollo 13']
    
    for movie in test_movies:
        print("Cast for " + movie + ": ")
        print(cast(movie))
        print('')
        
    print("=" * 60)


def test_graph():
    
    graph = make_graph()

    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(graph)
    print("" * 3)
    
    print("=" * 60)

    
######################## /tests ###################


    
test_actors()
test_cast()
#~ test_graph()


#~ shortest_path(make_graph(), 'Bacon, Kevin', 'Basinger, Kim')
#~ shortest_path(make_graph(), 'Bacon, Kevin', 'Buscemi, Steve')

