def NextStep(steps):
  for s in steps:
    if s != '-':
      return s + 1;
  return '-';

print NextStep([1,'-','-']);
print NextStep([1,'-','-']);
print NextStep(['-',1,'-']);
print NextStep(['-','-']);
print NextStep(['-','-','-']);
print NextStep([ 5, '-', 2 ]);
print NextStep([ '-' ]);
print NextStep([ 1 ]);
print NextStep([ 3, 2, 4 ]);

