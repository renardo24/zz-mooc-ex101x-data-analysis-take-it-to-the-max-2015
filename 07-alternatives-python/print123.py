def A_print123():
    print("1")
    print("2")
    print("3")
    
def B_print123():
    for i in range(3):
        print(i)
    
def C_print123():
    i=1
    while(i<4):
        print(i)
        i = i+1
 
def D_print123():
    for i in range(4):
        print(i)   

def E_print123():
    for i in range(1,4):
        print(i)      
    
