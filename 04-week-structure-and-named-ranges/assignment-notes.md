Frustration...(Just talk)
discussion posted a day ago by lindakt16
Man.....this has been the assignment I've struggled with most to date. And I couldn't even figure out how to solve it with formulas/named ranges/anything....I just had to assemble the info manually. Will definitely be waiting anxiously for the answers to be released, because I really want to understand how to combine all of this....I can tell it's right there at the edge of my brain, and I just can't quite grasp it.
I'm fairly certain you can keep the same formula we used to find the scheduled days of levels, and just replace x with a formula to return the right name...but everything I tried was simply returning errors. I know where the information I need is...just could not get my brain to work out a formula that would actually return a correct answer!
3 responses

Sydney2014_
a day ago
You are on the right path, as long as you have an intermediate spreadsheet to bring maximum level and availability together, you can use the index and match functions for the last part.

MichelleWissman
about 15 hours ago
I'm the same way and I use Excel constantly on a daily basis but more Pivot Tables and VLookups as my data is around numbers. I figured it out manually as formulas wouldn't work.

MichelleWissman
about 14 hours ago
I finally figured it out with a suggestion below. Create an intermediate table with teacher names in rows and days of week in columns. I used If statements and Vlookups to determine fill out the table based on each teacher and what level they can teach. Then on the schedule table, I replaced the "X" with an Index and Match function to the table created above with names of teachers. It was definitely not an easy thing at first. I feel like we jumped from basic simple things to stringing several things together that were more complicated. Plus I also feel that this assignment really had less to do with naming ranges than using various techniques from other videos.
Thank you MichelleWissman! Your comments helped me to figure out the solution. And agreed, this assignment feels like it's testing all that we've learnt thus far, rather than focusing on Named Ranges per se.
 
MATCH() function
discussion posted about an hour ago by Static23
The only problem with this was the MATCH() function. I could use it to check whether the first teacher who taught at that level was available on that day of the week. But it could not check subsequent matches, and Excel did not have a 2NDMATCH(), NEXTMATCH(), NMATCH(), or anything similar.
The result was that the formula I used did the following:
See if the first teacher who taught that level is available on that day. If not, then;
Arbitrarily start at row 7 and see if the next teacher who teaches at that level is available. If not, then;
Arbitrarily start at row 10 and see if the next teacher who teaches at that level is available.
The resulting "Scherzerade formula" has five "layers" of functions, and uses 16 functions total, such as FIND(), INDEX(), MATCH(), and IF(). The worst part, though, is that it only got 14 out of 15 answers correct.
I can imagine that there is a set of row starting points that will lead to 15 out of 15 correct.
In the meantime, though, there should be a FORMULALEN() function that tells us how ridiculously long your formulas are.

dhannon COMMUNITY TA
about an hour ago
Perhaps we can ask a VBA enthousiast to write a custom formula like this ;)
For the problem with the match, you could try creating an intermediate table which holds not if a teacher can teach on a certain day, but the max level a teacher can teach on a certain day.

Having a really hard time
question posted a day ago by WillEvo
Could any of the TA's offer some more advice for solving this problem? I am truly stuck and have no idea how to proceed.

WLitzenberg COMMUNITY TA
a day ago
Did you see the tip already provided for the question? "Consider creating an intermediate spreadsheet to bring maximum level and availability together."
Create a new intermediate table with the teachers and days of the week as your row and column names - inside the table if a teacher is available on a given day instead of using an X add the teachers maximum level. Can you move forward from here?
I did that, however I am still having a hard time seeing how to proceed. My first instinct is to write another formula to replace the X in the if statement on the schedule which will return a teachers name which is available that day at has that maximum level. Essentially the if statement will run my new formula if it was suppose to place an X in the schedule. Of course saying it and making it happen are two different things entirely.
I could do this in python easily with iterators, but excel is giving me trouble.
 
posted a day ago by WillEvo
Well, now at least we have a light. Tks
 
posted a day ago by Chico_sp
Hi WillEvo,
In creating your intermediate table, I would keep the structure of the two tables you already have so Teachers in the rows and days of week in columns. This intermediate table would be populated with the teacher's maximum level on the days he/she is available to teach. For your schedule you will refer to this intermediate table and use the formulas you've learned thus far to list the name of the (first) teacher that is available to teach that level of class.

