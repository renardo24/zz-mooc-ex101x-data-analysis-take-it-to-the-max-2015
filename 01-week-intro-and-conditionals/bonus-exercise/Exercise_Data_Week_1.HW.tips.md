
From: https://courses.edx.org/courses/DelftX/EX101x/1T2015/discussion/forum/2020842c727c0ce03735dae3b4d95c81c27d261e/threads/552584cb2a472d11030032eb

In the discussion you can see people using the MID() function but let's look at a feature of FIND(). In your first column you would have found the position of the first comma and grabbed the data to the LEFT of that - right?

But FIND has another property: FIND(search-string, source-string, start-pos) -- start-pos is the key here. You want the comma starting from after(+1) whatever the last comma number was . (see how + and -1 are going to recur for this stuff?).

Here's an example of the FIND() in action copied from my help manual for you:

=FIND(“e”, “where on earth”) returns 3 (“e” is the third character in the string “where on earth”).

=FIND(“e”, “where on earth”, 8) returns 10 (“e” in earth is the first “e” found starting from character 8, the “n” in “on”).

Now here's the thing - these commas are going to be all over the place so to make things easy you might want to start a new column and use it to store the number position of COMMA 1, beside that, have another column and use it to store the number position of COMMA 2.

Now think about this: if you know the numbers for where comma 1 and 2 are found then the characters between those numbers must be the second part of the data we need - in this case the TOWN. So let's say the first comma is 10 spaces 'in' and comma 2 is 20 spaces 'in' if you subtract comma 1 from comma 2 (20-10) you know the TOWN name is 10 characters long, hmmm, but there is a space after comma 1 so you need to consider that when we get to the next bit. You want the TOWN name not SPACE and TOWN name.

I'm not saying this is the correct and only way to go about this problem but it sure gets you thinking about how to tackle the problem.

Lastly, don't forget to explore the RIGHT() function. LEFT gets the characters to the left of a certain point, RIGHT will return you the values to the RIGHT of a certain point.

So, if you could get the characters to the RIGHT of the first comma AND you knew how long the length of the TOWN name was, you could find the LEFT x characters of everything to the RIGHT of the first comma and that would give you the TOWN name. Does that make sense?

Like I said, it's just one way to tackle this. There is a MID() function which you can use. And also there's a LEN() function which is very handy, it finds the total length or count of the characters and with subtraction etc. you can easily use that to split out the data you need.

In short, extracting strings means you will need to do some mathy stuff like we did to find the TOWN you need to work out where the town starts and where it ends and the commas are the key. To get the rest of the data we will need to FIND the " " after the second comma - keeping in mind the first space after the second comma is a SPACE so we need to look for the space after that one (+1).

That should get you started.

-----

This was more or less what I did too. Another approach would be to use just the left and the replace functions with some extra columns one can easily hide. Starting from the left side, gradually replacing the fields with "" until we're done. This approach would use some extra columns, but should be computationally more efficient. 
